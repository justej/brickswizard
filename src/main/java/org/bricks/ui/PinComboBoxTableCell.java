package org.bricks.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import org.bricks.model.Pin;
import org.bricks.ui.view.WizardController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by oliezhov on 8/23/2016.
 */
public class PinComboBoxTableCell extends TableCell<WizardController.BoardSetConnection, String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PinComboBoxTableCell.class);
    private final String boardName;
    private ComboBox<String> comboBox;

    public PinComboBoxTableCell(String boardName) {
        this.boardName = boardName;
    }

    @Override
    public void startEdit() {
        if (!isEmpty()) {
            super.startEdit();
            createCombobox();
            setItemProperties(null, comboBox);
        }
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setItemProperties(getValue(), null);
    }

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setItemProperties(null, null);
        } else {
            if (isEditing()) {
                if (comboBox != null) {
                    comboBox.setValue(getValue());
                }
                setItemProperties(getValue(), comboBox);
            } else {
                setItemProperties(getValue(), null);
            }
        }
    }

    private void setItemProperties(String text, Node graphic) {
        setText(text);
        setGraphic(graphic);
    }

    private void createCombobox() {
        WizardController.BoardSetConnection boardSetConnection = getTableView().getItems().get(getIndex());
        Pin pin = boardSetConnection.getBoardProperty(boardName).getValue();
        List<String> pinConfigs = pin.configs.stream().map(config -> config.getType() + ":" + config.getFunction()).collect(Collectors.toList());
        ObservableList<String> observablePinConfigs = FXCollections.observableList(pinConfigs);
        comboBox = new ComboBox<>(observablePinConfigs);
        comboBox.setPrefWidth(getTableColumn().getWidth());
        comboBox.valueProperty().set(getValue());
        comboBox.setOnAction(e -> {
            LOGGER.info("Committed: {}", comboBox.getSelectionModel().getSelectedItem());
            commitEdit(comboBox.getSelectionModel().getSelectedItem());
        });
    }

    private String getValue() {
        return getItem() == null ? "" : getItem();
    }
}
