package org.bricks.ui.view;

import com.google.common.collect.Iterables;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import org.bricks.model.Board;
import org.bricks.model.Pin;
import org.bricks.model.PinConfig;
import org.bricks.reader.BoardReader;
import org.bricks.reader.XmlBoardReader;
import org.bricks.ui.PinComboBoxTableCell;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

public class WizardController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WizardController.class);
    private static final BoardReader BOARD_READER = new XmlBoardReader();
    public static final String PIN_NUMBER_COLUMN_NAME = "Pin#";
    private static final String GPIO_COLUMN_NAME = "GPIO";

    @FXML
    private ComboBox<Board> cpuBoards;
    @FXML
    private ListView<Board> peripheralBoards;
    @FXML
    private TableView<BoardSetConnection> boardSetConfiguration;
    @FXML
    private TextArea logArea;

    public WizardController() {
    }

    public void loadBoards() {
        File path = new File(WizardController.class.getClassLoader().getResource("").getFile() + "xml/board");
        if (!path.exists() || !path.isDirectory()) {
            logError("Cannot load board configuration files");
            return;
        }

        ObservableList<Board> cpuBoardLIst = FXCollections.observableArrayList();
        ObservableList<Board> peripheralBoardList = FXCollections.observableArrayList();
        if (!readBoards(path, cpuBoardLIst, peripheralBoardList)) {
            return;
        }

        logLoadedBoards(Iterables.concat(cpuBoardLIst, peripheralBoardList));
        configureCpuBoards(cpuBoardLIst);
        configurePeripheralBoards(peripheralBoardList);
        configureBoardSetConfiguration(cpuBoardLIst, peripheralBoardList);
    }

    private void configureBoardSetConfiguration(Collection<Board> cpuBoards, Collection<Board> peripheralBoards) {
        boardSetConfiguration.getColumns().addAll(createColumns(cpuBoards, peripheralBoards));
        boardSetConfiguration.setEditable(true);
        Board cpuBoard = cpuBoards.iterator().next();
        populateBoardSetConnections(cpuBoard);
        boardSetConfiguration.getColumns().forEach(col -> enableBoardColumn(col, Collections.singleton(cpuBoard)));
    }

    private static void enableBoardColumn(TableColumn<BoardSetConnection, ?> column, Collection<Board> boards) {
        if (column.getText().equals(PIN_NUMBER_COLUMN_NAME) || column.getText().equals(GPIO_COLUMN_NAME) || boards.stream()
                .filter(board -> board.getName().equals(column.getText()))
                .findAny()
                .isPresent()) {
            column.setVisible(true);
        } else {
            column.setVisible(false);
        }
    }

    private static Collection<TableColumn<BoardSetConnection, String>> createColumns(Iterable<Board> cpuBoards, Iterable<Board> peripheralBoards) {
        Collection<TableColumn<BoardSetConnection, String>> columns = new ArrayList<>();
        columns.add(createColumn(PIN_NUMBER_COLUMN_NAME, WizardController::rowNumberCellValueFactory));
        columns.add(createColumn(GPIO_COLUMN_NAME, WizardController::gpioCellFactory));
        Iterable<Board> boards = Iterables.concat(cpuBoards, peripheralBoards);
        boards.forEach(board -> columns.add(createColumn(board.getName(), WizardController::comboBoxCellValueFactory)));
        return columns;
    }

    private static TableColumn<BoardSetConnection, String> createColumn(@NotNull String caption, @NotNull Callback<TableColumn.CellDataFeatures<BoardSetConnection, String>, ObservableValue<String>> cellValueFactory) {
        TableColumn<BoardSetConnection, String> column = new TableColumn<>(caption);
        column.setCellValueFactory(cellValueFactory);
        column.setCellFactory(cb -> new PinComboBoxTableCell(caption));
        column.setSortable(false);
        return column;
    }

    private void logLoadedBoards(Iterable<Board> boards) {
        logInfo("Loaded boards:");
        for (Board board : boards) {
            logInfo(board.getType() + " board: " + board.getName());
        }
    }

    private void populateBoardSetConnections(Board cpuBoard) {
        ObservableList<BoardSetConnection> boardSetConnections = FXCollections.observableArrayList();
        List<Pin> cpuBoardPins = cpuBoard.getAllPins();
        for (int i = 0; i < cpuBoardPins.size(); i++) {
            Map<String, SimpleObjectProperty<Pin>> boards = new HashMap<>(cpuBoardPins.size());
            boards.put(cpuBoard.getName(), new SimpleObjectProperty<>(cpuBoardPins.get(i)));
            for (Board board : peripheralBoards.getItems()) {
                // TODO: invoke getAllPins() only once per board
                boards.put(board.getName(), new SimpleObjectProperty<>(board.getAllPins().get(i)));
            }
            boardSetConnections.add(new BoardSetConnection(i + 1, cpuBoardPins.get(i).port, cpuBoardPins.get(i).pin, boards));
        }
        boardSetConfiguration.setItems(boardSetConnections);
    }

    @NotNull
    private static ObservableValue<String> rowNumberCellValueFactory(TableColumn.CellDataFeatures<BoardSetConnection, String> cellData) {
        return new SimpleStringProperty(String.valueOf(cellData.getValue().getPinNoProperty().getValue()));
    }

    @NotNull
    private static ObservableValue<String> gpioCellFactory(TableColumn.CellDataFeatures<BoardSetConnection, String> cellData) {
        String port = cellData.getValue().getPortProperty().getValue();
        if (port == null) {
            return new SimpleStringProperty("-");
        }

        Integer pin = cellData.getValue().getPinProperty().getValue();
        return new SimpleStringProperty(port + "." + pin);
    }

    @NotNull
    private static ObservableValue<String> comboBoxCellValueFactory(TableColumn.CellDataFeatures<BoardSetConnection, String> cellData) {
        String col = cellData.getTableColumn().getText();
        return new SimpleStringProperty(getPinConfigOption(cellData.getValue().getBoardProperty(col).get().configs, 0));
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Peripheral boards list view
    // -----------------------------------------------------------------------------------------------------------------

    private void configurePeripheralBoards(ObservableList<Board> boards) {
        peripheralBoards.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        peripheralBoards.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Collection<Board> selectedBoards = new ArrayList<>(peripheralBoards.getSelectionModel().getSelectedItems());
                selectedBoards.add(cpuBoards.getSelectionModel().getSelectedItem());
                boardSetConfiguration.getColumns().forEach(col -> enableBoardColumn(col, selectedBoards));
            }
        });
        peripheralBoards.setItems(boards);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // CPU board chombobox
    // -----------------------------------------------------------------------------------------------------------------

    private void configureCpuBoards(ObservableList<Board> cpuBoards) {
        this.cpuBoards.setItems(cpuBoards);

        if (this.cpuBoards.getItems().isEmpty() || this.cpuBoards.getItems().get(0) == null) {
            logInfo("CPU board list is empty");
        } else {
            this.cpuBoards.setValue(this.cpuBoards.getItems().get(0));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Other
    // -----------------------------------------------------------------------------------------------------------------

    private static boolean readBoards(File path, ObservableList<Board> cpuBoards, ObservableList<Board> peripheralBoards) {
        for (File file : path.listFiles()) {
            if (file.isFile() && file.getName().toLowerCase().endsWith("xml")) {
                try {
                    Board board = BOARD_READER.readBoard(new FileInputStream(file));
                    switch (board.getType()) {
                        case Board.CPU:
                            cpuBoards.add(board);
                            break;
                        case Board.PERIPHERAL:
                            peripheralBoards.add(board);
                            break;
                        default:
                            LOGGER.warn("Unknown board type {}. Ignoring.", board.getType());
                    }
                } catch (Throwable e) {
                    LOGGER.error("An error occured during processing file {}", file.getName());
                    return false;
                }
            }
        }
        return true;
    }

    private static String getPinConfigOption(List<PinConfig> pinConfigs, int index) {
        PinConfig pinConfig = pinConfigs.get(index);
        return pinConfig.getType() + ":" + pinConfig.getFunction();
    }

    private void logError(String message) {
        LOGGER.error(message);
        logArea.appendText("Error: " + message + "\n");
    }

    private void logWarning(String message) {
        LOGGER.warn(message);
        logArea.appendText("Warning: " + message + "\n");
    }

    private void logInfo(String message) {
        LOGGER.info(message);
        logArea.appendText("Info: " + message + "\n");
    }

    public static class BoardSetConnection {
        private final SimpleStringProperty port;
        private final SimpleIntegerProperty pin;
        private final SimpleIntegerProperty pinNo;
        private final Map<String, SimpleObjectProperty<Pin>> boards;

        private BoardSetConnection(int pinNo, String port, Integer pin, Map<String, SimpleObjectProperty<Pin>> boards) {
            this.pinNo = new SimpleIntegerProperty(pinNo);
            this.port = new SimpleStringProperty(port);
            this.pin = new SimpleIntegerProperty(pin == null ? -1 : pin);
            this.boards = boards;
        }

        public SimpleIntegerProperty getPinNoProperty() {
            return pinNo;
        }

        public Map<String, SimpleObjectProperty<Pin>> getBoards() {
            return boards;
        }

        public SimpleStringProperty getPortProperty() {
            return port;
        }

        public SimpleIntegerProperty getPinProperty() {
            return pin;
        }

        public SimpleObjectProperty<Pin> getBoardProperty(String boardName) {
            return boards.get(boardName);
        }
    }
}
