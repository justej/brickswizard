package org.bricks.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.bricks.ui.view.WizardController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JavaFxWizard extends Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(JavaFxWizard.class);
    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Bricks Wizard");
        initLayout();
    }

    private void initLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(WizardController.class.getResource("Wizard.fxml"));
            AnchorPane rootLayout = loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            WizardController controller = loader.getController();
            controller.loadBoards();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Cannot create layout");
        }
    }

    public static void run(String[] args) {
        launch(args);
    }
}
