package org.bricks.checker;

import org.bricks.model.Board;
import org.bricks.model.Pin;
import org.bricks.model.PinConfig;
import org.bricks.model.PinGroup;

import javax.annotation.Nonnull;
import java.util.AbstractMap.SimpleEntry;
import java.util.*;

import static org.bricks.model.PinConfig.*;

public class PinChecker {
    // Connection matrix of CPU board pins to peripheral board pins
    private static final Map<PinConfig, List<PinConfig>> CPU_TO_PERIPHERAL_CONNECTION_MATRIX = new HashMap<>();
    // Connection matrix of peripheral board pins to peripheral board pins
    private static final Map<PinConfig, List<PinConfig>> PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX = new HashMap<>();

    static {
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(NC,
                Arrays.asList(NC, POWER_GND, POWER_VBKUP, POWER_VCC, POWER_VBAT, POWER_5V, POWER_12V, GPIO_IN, GPIO_OUT,
                        ANALOG_IN, ANALOG_OUT, USART_RX, USART_TX, I2C_SDA, I2C_SCL, SPI_SS, SPI_MOSI, SPI_MISO,
                        SPI_SCK, CAN_RX, CAN_TX, USB_DM, USB_DP));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_GND, Arrays.asList(NC, POWER_GND));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_VBKUP, Arrays.asList(NC, POWER_VBKUP));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_VCC, Arrays.asList(NC, POWER_VCC));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_VBAT, Arrays.asList(NC, POWER_VBAT));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_5V, Arrays.asList(NC, POWER_5V));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_12V, Arrays.asList(NC, POWER_12V));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(GPIO, Arrays.asList(NC, GPIO_IN, GPIO_OUT, SPI_SS));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(ANALOG_IN, Arrays.asList(NC, ANALOG_OUT));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(ANALOG_OUT, Arrays.asList(NC, ANALOG_IN));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(USART_RX, Arrays.asList(NC, USART_TX));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(USART_TX, Arrays.asList(NC, USART_RX));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(I2C_SDA, Arrays.asList(NC, I2C_SDA));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(I2C_SCL, Arrays.asList(NC, I2C_SCL));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(SPI_SS, Arrays.asList(NC, SPI_SS, GPIO_OUT));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(SPI_MOSI, Arrays.asList(NC, SPI_MISO));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(SPI_MISO, Arrays.asList(NC, SPI_MOSI));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(SPI_SCK, Arrays.asList(NC, SPI_SCK));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(CAN_RX, Arrays.asList(NC, CAN_RX));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(CAN_TX, Arrays.asList(NC, CAN_TX));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(USB_DM, Arrays.asList(NC, USB_DM));
        CPU_TO_PERIPHERAL_CONNECTION_MATRIX.put(USB_DP, Arrays.asList(NC, USB_DP));

        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(NC,
                Arrays.asList(NC, POWER_GND, POWER_VBKUP, POWER_VCC, POWER_VBAT, POWER_5V, POWER_12V, GPIO_IN, GPIO_OUT,
                        ANALOG_IN, ANALOG_OUT, USART_RX, USART_TX, I2C_SDA, I2C_SCL, SPI_SS, SPI_MOSI, SPI_MISO,
                        SPI_SCK, CAN_RX, CAN_TX, USB_DM, USB_DP));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_GND, Arrays.asList(NC, POWER_GND));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_VBKUP, Arrays.asList(NC, POWER_VBKUP));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_VCC, Arrays.asList(NC, POWER_VCC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_VBAT, Arrays.asList(NC, POWER_VBAT));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_5V, Arrays.asList(NC, POWER_5V));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(POWER_12V, Arrays.asList(NC, POWER_12V));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(GPIO_IN, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(GPIO_OUT, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(ANALOG_IN, Arrays.asList(NC, ANALOG_IN));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(ANALOG_OUT, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(USART_RX, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(USART_TX, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(I2C_SDA, Arrays.asList(NC, I2C_SDA));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(I2C_SCL, Arrays.asList(NC, I2C_SCL));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(SPI_SS, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(SPI_MOSI, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(SPI_MISO, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(SPI_SCK, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(CAN_RX, Arrays.asList(NC, CAN_RX));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(CAN_TX, Arrays.asList(NC, CAN_TX));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(USB_DM, Arrays.asList(NC));
        PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX.put(USB_DP, Arrays.asList(NC));
    }

    public static Set<SimpleEntry<PinId, String>> check(@Nonnull Board cpuBoard,
                                                        @Nonnull List<Board> peripheralBoardSet) {
        final int groupsPerBoard = cpuBoard.getGroups().size();
        final int pinsPerGroup = cpuBoard.getGroup(0).getPinFunctionsList().size();
        Set<SimpleEntry<PinId, String>> conflicts = new HashSet<>();
        // Validate pins number
        for (Board board : peripheralBoardSet) {
            if (board.getGroups().size() != groupsPerBoard) {
                throw new IllegalArgumentException("Number of groups is not matched between boards");
            }
            for (PinGroup group : board.getGroups()) {
                if (group.getPinFunctionsList().size() != pinsPerGroup) {
                    throw new IllegalArgumentException("Number of pins is not matched between all boards");
                }
            }
        }
        // Validate pins
        for (int group = 0; group < groupsPerBoard; group++) {
            for (int pin = 0; pin < pinsPerGroup; pin++) {
                for (Board board : peripheralBoardSet) {
                    if (!checkConnection(cpuBoard.getGroup(group).getPinFunctions(pin),
                            board.getGroup(group).getPinFunctions(pin), CPU_TO_PERIPHERAL_CONNECTION_MATRIX)) {
                        conflicts.add(new SimpleEntry<>(new PinId(group, pin), formatCpuToPeripheralError(cpuBoard, board, new PinId(group, pin))));
                    }
                    for (Board anotherBoard : peripheralBoardSet) {
                        if (!anotherBoard.equals(board)) {
                            if (!checkConnection(anotherBoard.getGroup(group).getPinFunctions(pin),
                                    board.getGroup(group).getPinFunctions(pin),
                                    PERIPHERAL_TO_PERIPHERAL_CONNECTION_MATRIX)) {
                                conflicts.add(new SimpleEntry<>(new PinId(group, pin), formatPeripheralToPeripheralError(board, anotherBoard, new PinId(group, pin))));
                            }
                        }
                    }
                }
            }
        }
        return conflicts;
    }

    public static String formatPeripheralToPeripheralError(Board board1, Board board2, PinId pinId) {
        return String.format(
                "Conflict between peripheral pin %s (group %s) and peripheral pin %s (group %s)",
                board2.getGroup(pinId.getGroup()).getPinFunctions(pinId.getPin()),
                board2.getGroup(pinId.getGroup()).getId(),
                board1.getGroup(pinId.getGroup()).getPinFunctions(pinId.getPin()), board1.getGroup(pinId.getGroup()).getId());
    }

    public static String formatCpuToPeripheralError(Board board1, Board board2, PinId pinId) {
        return String.format("Conflict between CPU pin %s (group %s) and peripheral pin %s (group %s)",
                board1.getGroup(pinId.getGroup()).getPinFunctions(pinId.getPin()), board1.getGroup(pinId.getGroup()).getId(),
                board2.getGroup(pinId.getGroup()).getPinFunctions(pinId.getPin()), board2.getGroup(pinId.getGroup()).getId());
    }

    private static boolean checkConnection(Pin pins1, Pin pins2, Map<PinConfig, List<PinConfig>> connectionMatrix) {
        for (PinConfig pc1 : pins1.configs) {
            for (PinConfig pc2 : pins2.configs) {
                if (connectionMatrix.get(pc1).contains(pc2)) {
                    return true;
                }
            }
        }
        return false;
    }
}
