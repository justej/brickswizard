package org.bricks.checker;

import java.util.Objects;

public class PinId {
    private final int group;
    private final int pin;

    public PinId(int group, int pin) {
        super();
        this.group = group;
        this.pin = pin;
    }

    public int getGroup() {
        return group;
    }

    public int getPin() {
        return pin;
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, pin);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PinId pinId = (PinId) o;
        return group == pinId.group &&
                pin == pinId.pin;
    }
}
