package org.bricks.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Pin {
    public final String port;
    public final Integer pin;
    public final List<PinConfig> configs;

    public Pin(List<PinConfig> configs) {
        this(null, null, configs);
    }

    public Pin(String port, Integer pin, List<PinConfig> configs) {
        this.port = port;
        this.pin = pin;
        this.configs = configs;
    }

    @Override
    public String toString() {
        return configs.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pin pin = (Pin) o;
        return Objects.equals(configs, pin.configs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configs);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        List<PinConfig> pcList = new ArrayList<>();
        for (PinConfig pc : configs) {
            pcList.add(pc.clone());
        }
        return pcList;
    }
}
