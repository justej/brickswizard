package org.bricks.model;

import java.util.List;

public interface Board {
    String CPU = "cpu";
    String PERIPHERAL = "peripheral";

    String getName();

    String getType();

    String getDescription();

    String getVersion();

    List<PinGroup> getGroups();

    PinGroup getGroup(int num);

    void rotateCCW(int times);

    int getTurns();

    int getSubgroupsInGroup();

    List<Pin> getAllPins();
}
