package org.bricks.model;

import org.bricks.model.adapter.PinConfigAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Objects;

@XmlJavaTypeAdapter(PinConfigAdapter.class)
public class PinConfig {
    public static final PinConfig NC = new PinConfig("nc", "any");
    public static final PinConfig GPIO = new PinConfig("gpio", "any");
    public static final PinConfig GPIO_IN = new PinConfig("gpio", "in");
    public static final PinConfig GPIO_OUT = new PinConfig("gpio", "out");
    public static final PinConfig ANALOG_IN = new PinConfig("analog", "in");
    public static final PinConfig ANALOG_OUT = new PinConfig("analog", "out");
    public static final PinConfig USART_RX = new PinConfig("usart", "rx");
    public static final PinConfig USART_TX = new PinConfig("usart", "tx");
    public static final PinConfig I2C_SDA = new PinConfig("i2c", "sda");
    public static final PinConfig I2C_SCL = new PinConfig("i2c", "scl");
    public static final PinConfig SPI_SS = new PinConfig("spi", "ss");
    public static final PinConfig SPI_MOSI = new PinConfig("spi", "mosi");
    public static final PinConfig SPI_MISO = new PinConfig("spi", "miso");
    public static final PinConfig SPI_SCK = new PinConfig("spi", "sck");
    public static final PinConfig CAN_RX = new PinConfig("can", "rx");
    public static final PinConfig CAN_TX = new PinConfig("can", "tx");
    public static final PinConfig USB_DM = new PinConfig("usb", "dm");
    public static final PinConfig USB_DP = new PinConfig("usb", "dp");
    public static final PinConfig POWER_GND = new PinConfig("power", "gnd");
    public static final PinConfig POWER_VCC = new PinConfig("power", "vcc");
    public static final PinConfig POWER_VBAT = new PinConfig("power", "vbat");
    public static final PinConfig POWER_5V = new PinConfig("power", "+5v");
    public static final PinConfig POWER_12V = new PinConfig("power", "+12v");
    public static final PinConfig POWER_VBKUP = new PinConfig("power", "vbkup");

    private final String type;
    private final String function;

    public PinConfig(String type, String function) {
        super();
        this.type = type;
        this.function = function;
    }

    public String getType() {
        return type;
    }

    public String getFunction() {
        return function;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, function);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PinConfig pinConfig = (PinConfig) o;
        return Objects.equals(type, pinConfig.type) &&
                Objects.equals(function, pinConfig.function);
    }
    @Override
    public String toString() {
        return type + ":" + function;
    }

    @Override
    protected PinConfig clone() throws CloneNotSupportedException {
        return new PinConfig(type, function);
    }
}