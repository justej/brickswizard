package org.bricks.model.adapter;

import org.bricks.model.BoardImpl;
import org.bricks.model.PinGroup;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.List;

public class BoardImplAdapter extends XmlAdapter<BoardImplAdapter.AdaptedBoard, BoardImpl> {
    public static class Metadata {
        @XmlElement(name = "groups")
        private int groups;
        @XmlElement(name = "subgroups_in_group")
        private int subgroupsInGroup;
    }

    @XmlRootElement(name = "board")
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AdaptedBoard {
        private String name;
        private String type;
        private String description;
        private String version;
        @XmlElement(name = "metadata")
        private Metadata metadata;
        @XmlElement(name = "group")
        @XmlElementWrapper(name = "groups")
        private List<PinGroup> groups;
    }

    @Override
    public BoardImpl unmarshal(AdaptedBoard ab) throws Exception {
        if (ab.groups.size() != ab.metadata.groups) {
            throw new Exception("Declared and actual number of gropus doesn't match");
        }
        return new BoardImpl.Builder(ab.type, ab.groups, ab.metadata.subgroupsInGroup)
                .name(ab.name)
                .description(ab.description)
                .version(ab.version)
                .build();
    }

    @Override
    public AdaptedBoard marshal(BoardImpl board) throws Exception {
        throw new UnsupportedOperationException("Not implemented");
    }
}
