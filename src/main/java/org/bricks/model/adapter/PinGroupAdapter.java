package org.bricks.model.adapter;

import org.bricks.model.Pin;
import org.bricks.model.PinConfig;
import org.bricks.model.PinGroup;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.List;

public class PinGroupAdapter extends XmlAdapter<PinGroupAdapter.AdaptedPinGroup, PinGroup> {
    private static class HelperPin {
        @XmlAttribute
        private int id;
        @XmlAttribute
        private String type;
        @XmlAttribute
        private String function;
        @XmlAttribute
        private String port;
        @XmlAttribute(name = "pin")
        private Integer pinNo;
        @XmlElement
        private List<PinConfig> alt;
    }

    @XmlAccessorType(XmlAccessType.NONE)
    private static class Subgroup {
        @XmlAttribute
        private int id;
        @XmlElement(name = "pin")
        private HelperPin[] pins;

        public List<Pin> getSubgroupPins() {
            List<Pin> pinConfigs = new ArrayList<>(pins.length);
            for (int i = 0; i < pins.length; i++) {
                pinConfigs.add(null);
            }
            for (HelperPin pin : pins) {
                final int size = (pin.alt == null) ? 0 : pin.alt.size();
                final List<PinConfig> pinFunctions = new ArrayList<>(size + 1);
                pinFunctions.add(new PinConfig(pin.type, pin.function));
                if (pin.alt != null) {
                    pinFunctions.addAll(pin.alt);
                }
                pinConfigs.set(pin.id - 1, new Pin(pin.port, pin.pinNo, pinFunctions));
            }
            return pinConfigs;
        }

        public void setSubgroupPins(int subgroup, @Nonnull List<Pin> pinConfigs) {
            id = subgroup;
            HelperPin[] pins = new HelperPin[pinConfigs.size()];
            for (int i = 0; i < pins.length; i++) {
                final Pin pin = pinConfigs.get(i);
                pins[i] = new HelperPin();
                pins[i].id = i + 1;
                pins[i].type = pin.configs.get(0).getType();
                pins[i].function = pin.configs.get(0).getFunction();
                pins[i].alt = (pin.configs.size() > 1) ? pins[i].alt = new ArrayList<>(pin.configs.subList(1, pin.configs.size())) : null;
            }
            this.pins = pins;
        }
    }

    public static class AdaptedPinGroup {
        @XmlAttribute
        private String id;
        @XmlElement(name = "subgroup")
        private List<Subgroup> subgroups;

        public List<Pin> getPins() {
            final List<Pin> pins = new ArrayList<>();
            for (Subgroup sg : subgroups) {
                List<Pin> ps = sg.getSubgroupPins();
                pins.addAll(ps);
            }
            return pins;
        }

        public void setPins(@Nonnull PinGroup pinGroup, int pinsInSubgroup) {
            List<Pin> pinConfigs = pinGroup.getPinFunctionsList();
            List<Subgroup> subgroups = new ArrayList<>(pinConfigs.size());
            int pinNumber = 0;
            int subgroupNumber = 1;
            while (pinNumber < pinGroup.getPinFunctionsList().size()) {
                Subgroup subgroup = new Subgroup();
                subgroup.setSubgroupPins(subgroupNumber, pinConfigs.subList(pinNumber, pinNumber + pinsInSubgroup));
                subgroups.add(subgroup);
                pinNumber += pinsInSubgroup;
                subgroupNumber++;
            }
            this.subgroups = subgroups;
        }
    }

    @Override
    public PinGroup unmarshal(AdaptedPinGroup apg) throws Exception {
        return new PinGroup(apg.id, apg.subgroups.size(), apg.getPins());
    }

    @Override
    public AdaptedPinGroup marshal(PinGroup pg) throws Exception {
        AdaptedPinGroup apg = new AdaptedPinGroup();
        apg.id = pg.getId();
        final int pinsInSubgroup = pg.getPinFunctionsList().size() / pg.getSubgroupsInGroup();
        if (pinsInSubgroup * pg.getSubgroupsInGroup() != pg.getPinFunctionsList().size()) {
            throw new Exception("Declared and actual number of gropus doesn't match");
        }
        apg.setPins(pg, pinsInSubgroup);
        return apg;
    }
}
