package org.bricks.model.adapter;

import org.bricks.model.PinConfig;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class PinConfigAdapter extends XmlAdapter<PinConfigAdapter.AdaptedPinConfig, PinConfig> {
    public static class AdaptedPinConfig {
        @XmlAttribute
        private String type;
        @XmlAttribute
        private String function;
    }

    @Override
    public PinConfig unmarshal(AdaptedPinConfig apc) throws Exception {
        return new PinConfig(apc.type, apc.function);
    }

    @Override
    public AdaptedPinConfig marshal(PinConfig pc) throws Exception {
        AdaptedPinConfig apc = new AdaptedPinConfig();
        apc.type = pc.getType();
        apc.function = pc.getFunction();
        return apc;
    }
}
