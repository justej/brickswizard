package org.bricks.model;

import org.bricks.model.adapter.PinGroupAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@XmlJavaTypeAdapter(PinGroupAdapter.class)
public class PinGroup {
    private final String id;
    private final int subgroupsInGroup;
    private final List<Pin> pinFunctions;

    public PinGroup(String id, int subgroupsInGroup, List<Pin> pins) {
        this.id = id;
        this.subgroupsInGroup = subgroupsInGroup;
        this.pinFunctions = Collections.unmodifiableList(pins);
    }

    public String getId() {
        return id;
    }

    public int getSubgroupsInGroup() {
        return subgroupsInGroup;
    }

    public List<Pin> getPinFunctionsList() {
        return pinFunctions;
    }

    public Pin getPinFunctions(int index) {
        return pinFunctions.get(index);
    }

    @Override
    public String toString() {
        return "<id=" + id + ", pinFunctions=" + pinFunctions + ">";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PinGroup pinGroup = (PinGroup) o;
        return Objects.equals(id, pinGroup.id) &&
                Objects.equals(pinFunctions, pinGroup.pinFunctions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, pinFunctions);
    }
}
