package org.bricks.model;

import org.bricks.model.adapter.BoardImplAdapter;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@XmlRootElement
@XmlJavaTypeAdapter(BoardImplAdapter.class)
public class BoardImpl implements Board {
    private final String name;
    private final String type;
    private final String description;
    private final String version;
    private final int subgroupsInGroup;
    private List<PinGroup> groups;
    private int turns = 0;

    public static class Builder {
        private final List<PinGroup> groups;
        private String name;
        private String type;
        private String description;
        private String version;
        private int subgroupsInGroup;

        public Builder(String type, List<PinGroup> groups, int subgroupsInGroup) {
            this.type = type;
            this.groups = Collections.unmodifiableList(groups);
            this.subgroupsInGroup = subgroupsInGroup;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public BoardImpl build() {
            return new BoardImpl(this);
        }
    }

    private BoardImpl(Builder builder) {
        this.name = builder.name;
        this.type = builder.type;
        this.description = builder.description;
        this.version = builder.version;
        this.subgroupsInGroup = builder.subgroupsInGroup;
        this.groups = builder.groups;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public int getSubgroupsInGroup() {
        return subgroupsInGroup;
    }

    @Override
    public List<Pin> getAllPins() {
        return groups.stream().flatMap(group -> group.getPinFunctionsList().stream()).collect(Collectors.toList());
    }

    @Override
    public List<PinGroup> getGroups() {
        return groups;
    }

    @Override
    public PinGroup getGroup(int index) {
        return groups.get(index);
    }

    @Override
    public void rotateCCW(int times) {
        if (times < 0) {
            throw new IllegalArgumentException("Argument must be non-negative integer");
        }

        int size = groups.size();
        int newTurns = (turns + times) % size;
        if (turns == newTurns) {
            return;
        }
        List<PinGroup> g = new ArrayList<>(size);
        for (int i = 0, groupNum = newTurns - turns + size; i < size; i++, groupNum++) {
            g.add(groups.get(groupNum % size));
        }
        groups = Collections.unmodifiableList(g);
        turns = newTurns;
    }

    @Override
    public int getTurns() {
        return turns;
    }

    @Override
    public String toString() {
        return name;
    }
}
