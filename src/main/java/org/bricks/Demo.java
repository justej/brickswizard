package org.bricks;

import org.bricks.model.Board;
import org.bricks.model.Pin;
import org.bricks.model.PinGroup;
import org.bricks.reader.XmlBoardReader;
import org.bricks.ui.JavaFxWizard;

import java.io.*;

public class Demo {
    public static void main(String[] args) {
        try {
            // printConfiguration(args);
            runGui(args);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private static void runGui(String[] args) {
        new JavaFxWizard().run(args);
    }

    public static void printConfiguration(String[] args) throws IOException {
        Board board;
        File cpuFile = new File("src/main/resources/xml/board/Stm32f103c.xml");
        // File peripheralFile = new
        // File("src/main/resources/xml/board/Orientation.xml");
        // List<Integer> list = new ArrayList<>();
        // for (int i = 0; i < 400; i++) {
        // list.add(i);
        // }
        // list.set(49, null);
        // byte[] b = new byte[255];
        // System.in.read(b);
        // @NonNull Integer i = list.get(b[0]);
        try {
            board = new XmlBoardReader().readBoard(new FileInputStream(cpuFile));
            printXml(board);
            System.out.println("\n\n\n");
            // Pin[] plCpu = xmlReader.unmarshal(new FileInputStream(cpuFile));
            // Pin[] plPeripheral = xmlReader.unmarshal(new
            // FileInputStream(peripheralFile));
            // for (Pin p : plCpu) {
            // System.out.print(p + "; ");
            // }
            // System.out.println("");
            // for (Pin p : plPeripheral) {
            // System.out.print(p + "; ");
            // }
        } catch (FileNotFoundException ex) {
            try {
                System.err.println("Cannot open file " + cpuFile.getCanonicalPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void printXml(Board board) {
        final PrintStream ps = System.out;
        ps.println("Board name: " + board.getName());
        ps.println("Board type: " + board.getType());
        ps.println("Board version: " + board.getVersion());
        ps.println("Board description: " + board.getDescription());
        for (PinGroup pinGroup : board.getGroups()) {
            ps.println("group ID=" + pinGroup.getId());
            int pinNo = 0;
            for (Pin p : pinGroup.getPinFunctionsList()) {
                pinNo++;
                ps.println("\tID=" + pinNo + ": " + p);
            }
        }
    }
}
