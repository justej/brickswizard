package org.bricks.reader;

import org.bricks.model.Board;

import java.io.InputStream;

public interface BoardReader {
    Board readBoard(InputStream inputStream) throws Exception;
}
