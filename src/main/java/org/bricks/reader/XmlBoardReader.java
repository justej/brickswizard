package org.bricks.reader;

import org.bricks.model.Board;
import org.bricks.model.BoardImpl;
import org.bricks.model.adapter.BoardImplAdapter;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class XmlBoardReader implements BoardReader {
    private static final String XSD_FILE_PATH = "config/schema/bricks.xsd";
    private static final File SCHEMA_FILE = new File(XmlBoardReader.class.getClassLoader().getResource(XSD_FILE_PATH).getFile());

    @Override
    public Board readBoard(InputStream inputStream) throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(BoardImplAdapter.AdaptedBoard.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        XmlAdapter<BoardImplAdapter.AdaptedBoard, BoardImpl> boardAdapter = new BoardImplAdapter();
        BoardImplAdapter.AdaptedBoard adaptedBoard = (BoardImplAdapter.AdaptedBoard) unmarshaller.unmarshal(inputStream);
        validateXml(adaptedBoard, jaxbContext);
        return boardAdapter.unmarshal(adaptedBoard);
    }

    private void validateXml(BoardImplAdapter.AdaptedBoard board, JAXBContext jaxbContext) throws Exception {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(SCHEMA_FILE);
        JAXBSource boardSource = new JAXBSource(jaxbContext, board);
        Validator validator = schema.newValidator();
        validator.setErrorHandler(null);
        try {
            validator.validate(boardSource);
        } catch (SAXException | IOException e) {
            throw new Exception("File doesn't comply XSD", e);
        }
    }
}
