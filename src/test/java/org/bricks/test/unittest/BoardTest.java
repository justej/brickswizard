package org.bricks.test.unittest;

import static org.bricks.model.PinConfig.I2C_SCL;
import static org.bricks.model.PinConfig.I2C_SDA;
import static org.bricks.model.PinConfig.SPI_MISO;
import static org.bricks.model.PinConfig.SPI_MOSI;
import static org.bricks.model.PinConfig.SPI_SCK;
import static org.bricks.model.PinConfig.SPI_SS;
import static org.bricks.model.PinConfig.USART_RX;
import static org.bricks.model.PinConfig.USART_TX;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bricks.model.Board;
import org.bricks.model.BoardImpl;
import org.bricks.model.Pin;
import org.bricks.model.PinGroup;
import org.junit.Before;
import org.junit.Test;

public class BoardTest {
	
	private List<PinGroup> groups0deg;
	private List<PinGroup> groups90deg;

	@Before
	public void startUp() {
		final List<PinGroup> g1 = new ArrayList<>();
		g1.add(new PinGroup("1", 2, Arrays.asList(new Pin(Collections.singletonList(USART_RX)), new Pin(Collections.singletonList(USART_TX)))));
		g1.add(new PinGroup("2", 2, Arrays.asList(new Pin(Collections.singletonList(SPI_SS)), new Pin(Collections.singletonList(SPI_MISO)))));
		g1.add(new PinGroup("3", 2, Arrays.asList(new Pin(Collections.singletonList(SPI_MOSI)), new Pin(Collections.singletonList(SPI_SCK)))));
		g1.add(new PinGroup("4", 2, Arrays.asList(new Pin(Collections.singletonList(I2C_SDA)), new Pin(Collections.singletonList(I2C_SCL)))));
		groups0deg = Collections.unmodifiableList(g1);

		final List<PinGroup> g2 = new ArrayList<>();
		g2.add(new PinGroup("2", 2, Arrays.asList(new Pin(Collections.singletonList(SPI_SS)), new Pin(Collections.singletonList(SPI_MISO)))));
		g2.add(new PinGroup("3", 2, Arrays.asList(new Pin(Collections.singletonList(SPI_MOSI)), new Pin(Collections.singletonList(SPI_SCK)))));
		g2.add(new PinGroup("4", 2, Arrays.asList(new Pin(Collections.singletonList(I2C_SDA)), new Pin(Collections.singletonList(I2C_SCL)))));
		g2.add(new PinGroup("1", 2, Arrays.asList(new Pin(Collections.singletonList(USART_RX)), new Pin(Collections.singletonList(USART_TX)))));
		groups90deg = Collections.unmodifiableList(g2);
	}

	@Test
	public void testRotationPlus90Deg() {
		final Board testBoard = new BoardImpl.Builder(Board.PERIPHERAL, groups0deg, 2).build();
		final Board board90deg = new BoardImpl.Builder(Board.PERIPHERAL, groups90deg, 2).build();

		testBoard.rotateCCW(1);
		assertEquals(1, testBoard.getTurns());
		assertEquals(board90deg.getGroups(), testBoard.getGroups());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRotationMinus90Deg() {
		final Board testBoard = new BoardImpl.Builder(Board.PERIPHERAL, groups0deg, 2).build();

		testBoard.rotateCCW(-1);
	}

	@Test
	public void testRotation360Deg() {
		final Board testBoard = new BoardImpl.Builder(Board.PERIPHERAL, groups0deg, 2).build();
		final Board board90deg = new BoardImpl.Builder(Board.PERIPHERAL, groups90deg, 2).build();

		testBoard.rotateCCW(1);
		assertEquals(1, testBoard.getTurns());
		testBoard.rotateCCW(4);
		assertEquals(1, testBoard.getTurns());
		assertEquals(board90deg.getGroups(), testBoard.getGroups());
	}

}