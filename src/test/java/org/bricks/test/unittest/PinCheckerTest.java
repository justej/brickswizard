package org.bricks.test.unittest;

import static org.bricks.model.PinConfig.I2C_SCL;
import static org.bricks.model.PinConfig.I2C_SDA;
import static org.bricks.model.PinConfig.NC;
import static org.bricks.model.PinConfig.USART_RX;
import static org.bricks.model.PinConfig.USART_TX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;

import org.bricks.checker.PinChecker;
import org.bricks.checker.PinId;
import org.bricks.model.*;
import org.junit.Test;

public class PinCheckerTest {

	private static final List<PinConfig> nc = Arrays.asList(NC);
	private static final List<PinConfig> usartTx = Arrays.asList(USART_TX);
	private static final List<PinConfig> usartRx = Arrays.asList(USART_RX);
	private static final List<PinConfig> i2cSda = Arrays.asList(I2C_SDA);
	private static final List<PinConfig> i2cScl = Arrays.asList(I2C_SCL);

	private static final List<PinConfig> usartTx_i2cSda = Arrays.asList(USART_TX, I2C_SDA);
	private static final List<PinConfig> usartRx_i2cScl = Arrays.asList(USART_RX, I2C_SCL);
	
	private static final PinId group0Pin0 = new PinId(0, 0);
	private static final PinId group0Pin1 = new PinId(0, 1);
	private static final PinId group1Pin0 = new PinId(1, 0);
	private static final PinId group1Pin1 = new PinId(1, 1);

	private List<PinGroup> createBoardGroups(String[] groupIds, int subgroupsInGroup, List<List<PinConfig>> pinList) {
		final int pinsPerGroup = pinList.size() / groupIds.length;
		final List<PinGroup> groups = new ArrayList<>(groupIds.length);
		int pinNo = 0;
		for (String groupId : groupIds) {
			final List<Pin> side = new ArrayList<>(pinsPerGroup);
			for (int p = 0; p < pinsPerGroup; p++, pinNo++) {
				side.add(new Pin(pinList.get(pinNo)));
			}
			groups.add(new PinGroup(groupId, subgroupsInGroup, side));
		}
		return groups;
	}

	@Test
	public void testPinsWithMultipleValues() {
		final String[] cpuGroupIds = new String[] {"left", "bottom", "right", "top"};
		final List<List<PinConfig>> cpuPins = Arrays.asList(
				usartTx_i2cSda, usartRx_i2cScl,
				usartRx_i2cScl, usartTx_i2cSda,
				nc, nc,
				nc, nc
		);
		final List<PinGroup> cpuGroups = createBoardGroups(cpuGroupIds, 2, cpuPins);

		final String[] peripheralGroupIds = new String[] {"1", "2", "3", "4"};
		final List<List<PinConfig>> peripheralPins = Arrays.asList(
				usartRx, usartTx,
				i2cScl, i2cSda,
				nc, nc,
				nc, nc
		);
		final List<PinGroup> peripheralGroups = createBoardGroups(peripheralGroupIds, 2, peripheralPins);

		Board cpuBoard = new BoardImpl.Builder(Board.CPU, cpuGroups, 2).build();
		List<Board> peripheralBoardSet = Collections.singletonList(new BoardImpl.Builder(Board.PERIPHERAL, peripheralGroups, 2).build());

		Set<SimpleEntry<PinId, String>> checkResult = PinChecker.check(cpuBoard, peripheralBoardSet);
		assertTrue(checkResult.isEmpty());
	}

	@Test
	public void testPinsWithoutConflicts() {
		final String[] cpuGroupIds = new String[] {"left", "bottom", "right", "top"};
		final List<List<PinConfig>> cpuPins = Arrays.asList(
				usartTx, usartRx,
				i2cSda, i2cScl,
				i2cSda, i2cScl,
				nc, nc
		);
		final List<PinGroup> cpuGroups = createBoardGroups(cpuGroupIds, 2, cpuPins);

		final String[] peripheralGroupIds = new String[] {"1", "2", "3", "4"};
		final List<List<PinConfig>> peripheralPins = Arrays.asList(
				usartRx, usartTx,
				i2cSda, i2cScl,
				nc, nc,
				nc, nc
		);
		final List<PinGroup> peripheralGroups = createBoardGroups(peripheralGroupIds, 2, peripheralPins);

		Board cpuBoard = new BoardImpl.Builder(Board.CPU, cpuGroups, 2).build();
		List<Board> peripheralBoardSet = Collections.singletonList(new BoardImpl.Builder(Board.PERIPHERAL, peripheralGroups, 2).build());
		
		assertTrue(PinChecker.check(cpuBoard, peripheralBoardSet).isEmpty());
	}

	@Test
	public void testCpuToPeripheralConflicts() {
		final String[] cpuGroupIds = new String[] {"left", "bottom", "right", "top"};
		final List<List<PinConfig>> cpuPins = Arrays.asList(
				usartTx, usartRx,
				i2cSda, i2cScl,
				i2cSda, i2cScl,
				nc, nc
		);
		final List<PinGroup> cpuGroups = createBoardGroups(cpuGroupIds, 2, cpuPins);

		final String[] peripheralGroupIds = new String[] {"1", "2", "3", "4"};
		final List<List<PinConfig>> peripheralPins = Arrays.asList(
				usartTx, usartRx,
				i2cScl, i2cSda,
				nc, nc,
				nc, nc
		);
		final List<PinGroup> peripheralGroups = createBoardGroups(peripheralGroupIds, 2, peripheralPins);

		Board cpuBoard = new BoardImpl.Builder(Board.CPU, cpuGroups, 2).build();
		List<Board> peripheralBoardSet = Collections.singletonList(new BoardImpl.Builder(Board.PERIPHERAL, peripheralGroups, 2).build());

		Set<SimpleEntry<PinId, String>> conflicts = PinChecker.check(cpuBoard, peripheralBoardSet);
		assertEquals(4, conflicts.size());
		assertTrue(conflicts.contains(new AbstractMap.SimpleEntry<>(group0Pin0, PinChecker.formatCpuToPeripheralError(cpuBoard, peripheralBoardSet.get(0), group0Pin0))));
		assertTrue(conflicts.contains(new AbstractMap.SimpleEntry<>(group0Pin1, PinChecker.formatCpuToPeripheralError(cpuBoard, peripheralBoardSet.get(0), group0Pin1))));
		assertTrue(conflicts.contains(new AbstractMap.SimpleEntry<>(group1Pin0, PinChecker.formatCpuToPeripheralError(cpuBoard, peripheralBoardSet.get(0), group1Pin0))));
		assertTrue(conflicts.contains(new AbstractMap.SimpleEntry<>(group1Pin1, PinChecker.formatCpuToPeripheralError(cpuBoard, peripheralBoardSet.get(0), group1Pin1))));
	}

	@Test
	public void testPeripheralToPeripheralConflicts() {
		final String[] cpuGroupIds = new String[] {"left", "bottom", "right", "top"};
		final List<List<PinConfig>> cpuPins = Arrays.asList(
				nc, nc,
				nc, nc,
				nc, nc,
				nc, nc
		);
		final List<PinGroup> cpuGroups = createBoardGroups(cpuGroupIds, 2, cpuPins);

		final String[] peripheralGroupIds = new String[] {"1", "2", "3", "4"};
		final List<List<PinConfig>> peripheral1Pins = Arrays.asList(
				usartTx, usartRx,
				i2cScl, i2cSda,
				nc, nc,
				nc, nc
		);
		final List<PinGroup> peripheral1Groups = createBoardGroups(peripheralGroupIds, 2, peripheral1Pins);

		final String[] peripheral2GroupIds = new String[] {"1", "2", "3", "4"};
		final List<List<PinConfig>> peripheral2Pins = Arrays.asList(
				usartTx, usartRx,
				i2cScl, i2cSda,
				nc, nc,
				nc, nc
		);
		final List<PinGroup> peripheral2Groups = createBoardGroups(peripheral2GroupIds, 2, peripheral2Pins);

		Board cpuBoard = new BoardImpl.Builder(Board.CPU, cpuGroups, 2).build();
		Board peripheralBoard1 = new BoardImpl.Builder(Board.PERIPHERAL, peripheral1Groups, 2).build();
		Board peripheralBoard2 = new BoardImpl.Builder(Board.PERIPHERAL, peripheral2Groups, 2).build();
		List<Board> peripheralBoardSet = Arrays.asList(peripheralBoard1, peripheralBoard2);

		Set<SimpleEntry<PinId, String>> conflicts = PinChecker.check(cpuBoard, peripheralBoardSet);
		assertEquals(2, conflicts.size());
		assertTrue(conflicts.contains(new AbstractMap.SimpleEntry<>(group0Pin0, PinChecker.formatPeripheralToPeripheralError(peripheralBoard1, peripheralBoard2, group0Pin0))));
		assertTrue(conflicts.contains(new AbstractMap.SimpleEntry<>(group0Pin1, PinChecker.formatPeripheralToPeripheralError(peripheralBoard1, peripheralBoard2, group0Pin1))));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBoardsOfDifferentSize() {
		final String[] cpuGroupIds = new String[] {"1", "2", "3"};
		final List<List<PinConfig>> cpuPins = Arrays.asList(
				nc, nc,
				nc, nc,
				nc, nc
		);
		final List<PinGroup> cpuGroups = createBoardGroups(cpuGroupIds, 2, cpuPins);

		final String[] peripheralGroupIds = new String[] {"left", "bottom", "right", "top"};
		final List<List<PinConfig>> peripheralPins = Arrays.asList(
				usartTx, usartRx,
				i2cScl, i2cSda,
				nc, nc,
				nc, nc
		);
		final List<PinGroup> peripheralGroups = createBoardGroups(peripheralGroupIds, 2, peripheralPins);

		Board cpuBoard = new BoardImpl.Builder(Board.CPU, cpuGroups, 2).build();
		List<Board> peripheralBoardSet = Collections.singletonList(new BoardImpl.Builder(Board.PERIPHERAL, peripheralGroups, 2).build());
		PinChecker.check(cpuBoard, peripheralBoardSet);
	}
	
}
